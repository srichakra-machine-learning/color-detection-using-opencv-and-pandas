# OpenCV Example: Color detection using OpenCV and Pandas

The colors.csv file includes 865 color names along with their RGB and hex values.

The jpg file used in this example, also published with this repository.

Command Prompt:
python color_detection.py -i <add your image path here>